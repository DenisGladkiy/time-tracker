package com.denis.timetracking.exception;

/**
 * Created by Denis on 01.05.2018.
 */
public class IncorrectInputException extends Exception {

    public IncorrectInputException(String message){
        super(message);
    }

}
